# Instalation PXE server on raspberry
# ----------------------------------- 

# Resource: https://linuxconfig.org/how-to-configure-a-raspberry-pi-as-a-pxe-boot-server
#----------

sudo apt-get update
sudo apt-get install -y dnsmasq 
sudo apt-get install -y pxelinux 
sudo apt-get install -y syslinux-efi


sudo mkdir -p /mnt/data/netboot/{bios,efi64}

# Copy syslinux libraries to work
# -------------------------------
sudo cp \
  /usr/lib/syslinux/modules/bios/{ldlinux,vesamenu,libcom32,libutil}.c32 \
  /usr/lib/PXELINUX/pxelinux.0 \
  /mnt/data/netboot/bios

sudo cp \
  /usr/lib/syslinux/modules/efi64/ldlinux.e64 \
  /usr/lib/syslinux/modules/efi64/{vesamenu,libcom32,libutil}.c32 \
  /usr/lib/SYSLINUX.EFI/efi64/syslinux.efi \
  /mnt/data/netboot/efi64

# Create the directory that will host the distributions we want to make available in our boot menu
# ------------------------------------------------------------------------------------------------

sudo mkdir /mnt/data/netboot/boot

# Create the appropriate path with naming directories after the architecture, name and version of the system we want to provide
# -----------------------------------------------------------------------------------------------------------------------------

sudo mkdir -p /mnt/data/netboot/boot/amd64/debian/


sudo mkdir /mnt/data/isos
sudo wget https://cdimage.debian.org/debian-cd/11.2.0/amd64/iso-cd/debian-11.2.0-amd64-netinst.iso -O /mnt/data/isos/debian.iso 


# Mount iso distribution to copy files
# ------------------------------------

sudo mount -o loop -t iso9660 /mnt/data/isos/debian.iso /media

# Copy distribution files
# -----------------------
sudo mount -o loop -t iso9660 /mnt/data/isos/debian.iso /media

sudo rsync -av /media/ /mnt/data/netboot/boot/amd64/debian/

sudo umount /media

# Creating the boot menu
# ----------------------

sudo mkdir /mnt/data/netboot/pxelinux.cfg

sudo nano /mnt/data/netboot/pxelinux.cfg/default

#MENU TITLE  PXE Boot Menu
#DEFAULT     vesamenu.c32
#
#    LABEL local
#        MENU LABEL Boot from local drive
#        LOCALBOOT 0xffff
#
#    MENU BEGIN amd64
#    MENU TITLE amd64
#
#        MENU BEGIN Debian
#        MENU TITLE Debian
#
#            LABEL installgui
#                MENU LABEL ^Graphical install
#                KERNEL ::boot/amd64/debian/install.amd/vmlinuz
#                APPEND vga=788 initrd=::boot/amd64/debian/install.amd/gtk/initrd.gz --- quiet
#
#
#            LABEL install
#                MENU LABEL ^Install
#                KERNEL ::boot/amd64/debian/install.amd/vmlinuz
#                APPEND vga=788 initrd=::boot/amd64/debian/install.amd/initrd.gz --- quiet
#
#            MENU END
#
#    MENU END

cd /mnt/data/netboot
sudo ln -rs pxelinux.cfg bios
sudo ln -rs pxelinux.cfg efi64


# Configure dnsmasq
# -----------------
sudo mv /etc/dnsmasq.conf /etc/dnsmasq.conf.old
sudo nano /etc/dnsmasq.conf

port=0
interface=wlan0
# If your dhcp support pxe message you can use proxy dhcp
# dhcp-range=192.168.1.0,proxy 
# If your dhcp not support pxe message 
dhcp-range=192.168.1.1,192.168.1.253,255.255.255.0,6h
enable-tftp
tftp-root=/mnt/data/netboot
pxe-service=x86PC,"PXELINUX (BIOS)",bios/pxelinux
pxe-service=x86-64_EFI,"PXELINUX (EFI)",efi64/syslinux.efi
log-queries
log-facility=/var/log/dnsmasq.log
dhcp-boot=pxelinux.0,pxeserver,192.168.1.5
dhcp-authoritative
dhcp-option=option:router,192.168.1.254
dhcp-option=option:dns-server,192.168.1.254
# Affect a static ip to the host with mac adress b8:27:eb:3b:28:38
dhcp-host=b8:27:eb:3b:28:38,192.168.1.3


sudo systemctl restart dnsmasq

# Open ports (not needed on raspberry)
# ------------------------------------

sudo ufw allow 67/udp  		# port DHCP(server)
sudo ufw allow 69/udp  		# port TFTP 
sudo ufw allow 4011/udp 	# PXE

# You need to disable your dhcp server on router if it's not support PXE message in DHCP request
# In freebox router go to "paramètres de freebox"->"mode avancé"->"DHCP"-> disable "activer le serveur DHCP"
# ----------------------------------------------------------------------------------------------------------

# Set a static ip to the raspberry:
# ---------------------------------

# Resource: https://www.ionos.fr/digitalguide/serveur/configuration/allouer-a-raspberry-pi-une-adresse-ip-fixe/
# ---------

sudo nano /etc/dhcpcd.conf


# # Example static IP configuration:
# interface wlan0
# static ip_address=192.168.1.5/24
# static routers=192.168.1.254
# static domain_name_servers=192.168.1.254 8.8.8.8 fd51:42f8:caae:d92e::1

sudo reboot


# Util cmd:
# ---------

# To see affected ip by dnsmasq dhcp:
# -----------------------------------
cat /var/lib/misc/dnsmasq.leases

# To see dhcp request: 
#---------------------
sudo tcpdump -i wlan0 port 67 

